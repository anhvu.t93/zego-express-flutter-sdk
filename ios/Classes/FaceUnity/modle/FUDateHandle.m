//
//  FUDateHandle.m
//  FULiveDemo
//
//  Created by 孙慕 on 2020/6/20.
//  Copyright © 2020 FaceUnity. All rights reserved.
//

#import "FUDateHandle.h"
#import "FUBeautyParam.h"

@implementation FUDateHandle


+(NSArray<FUBeautyParam *>*)setupFilterData{
    NSArray *beautyFiltersDataSource = @[@"origin",@"ziran1",@"ziran2",@"ziran3",@"ziran4",@"ziran5",@"ziran6",@"ziran7",@"ziran8",
    @"zhiganhui1",@"zhiganhui2",@"zhiganhui3",@"zhiganhui4",@"zhiganhui5",@"zhiganhui6",@"zhiganhui7",@"zhiganhui8",@"bailiang1",@"bailiang2",@"bailiang3",@"bailiang4",@"bailiang5",@"bailiang6",@"bailiang7"
                                         ,@"fennen1",@"fennen2",@"fennen3",@"fennen5",@"fennen6",@"fennen7",@"fennen8",
                                         @"lengsediao1",@"lengsediao2",@"lengsediao3",@"lengsediao4",@"lengsediao7",@"lengsediao8",@"lengsediao11",
                                         @"nuansediao1",@"nuansediao2",
                                         @"gexing1",@"gexing2",@"gexing3",@"gexing4",@"gexing5",@"gexing7",@"gexing10",@"gexing11",
                                         @"xiaoqingxin1",@"xiaoqingxin3",@"xiaoqingxin4",@"xiaoqingxin6",
                                         @"heibai1",@"heibai2",@"heibai3",@"heibai4"];
    
    NSDictionary *filtersCHName = @{@"origin":@"origin",
                                    @"bailiang1":@"White_Bright_1",
                                    @"bailiang2":@"White_Bright_2",
                                    @"bailiang3":@"White_Bright_3",
                                    @"bailiang4":@"White_Bright_4",
                                    @"bailiang5":@"White_Bright_5",
                                    @"bailiang6":@"White_Bright_6",
                                    @"bailiang7":@"White_Bright_7",
                                    @"fennen1":@"Matte_1",
                                    @"fennen2":@"Matte_2",
                                    @"fennen3":@"Matte_3",
                                    @"fennen4":@"Matte_4",
                                    @"fennen5":@"Matte_5",
                                    @"fennen6":@"Matte_6",
                                    @"fennen7":@"Matte_7",
                                    @"fennen8":@"Matte_8",
                                    @"gexing1":@"Personality_1",
                                    @"gexing2":@"Personality_2",
                                    @"gexing3":@"Personality_3",
                                    @"gexing4":@"Personality_4",
                                    @"gexing5":@"Personality_5",
                                    @"gexing6":@"Personality_6",
                                    @"gexing7":@"Personality_7",
                                    @"gexing8":@"Personality_8",
                                    @"gexing9":@"Personality_9",
                                    @"gexing10":@"Personality_10",
                                    @"gexing11":@"Personality_11",
                                    @"heibai1":@"Black_White_1",
                                    @"heibai2":@"Black_White_2",
                                    @"heibai3":@"Black_White_3",
                                    @"heibai4":@"Black_White_4",
                                    @"heibai5":@"Black_White_5",
                                    @"lengsediao1":@"Cool_Colors_1",
                                    @"lengsediao2":@"Cool_Colors_2",
                                    @"lengsediao3":@"Cool_Colors_3",
                                    @"lengsediao4":@"Cool_Colors_4",
                                    @"lengsediao5":@"Cool_Colors_5",
                                    @"lengsediao6":@"Cool_Colors_6",
                                    @"lengsediao7":@"Cool_Colors_7",
                                    @"lengsediao8":@"Cool_Colors_8",
                                    @"lengsediao9":@"Cool_Colors_9",
                                    @"lengsediao10":@"Cool_Colors_10",
                                    @"lengsediao11":@"Cool_Colors_11",
                                    @"nuansediao1":@"Warm_Tones_1",
                                    @"nuansediao2":@"Warm_Tones_2",
                                    @"nuansediao3":@"Warm_Tones_3",
                                    @"xiaoqingxin1":@"Colorful_1",@"xiaoqingxin2":@"Colorful_2",@"xiaoqingxin3":@"Colorful_3",@"xiaoqingxin4":@"Colorful_4",@"xiaoqingxin5":@"Colorful_5",@"xiaoqingxin6":@"Colorful_6",
                                    @"ziran1":@"Natural_1",@"ziran2":@"Natural_2",@"ziran3":@"Natural_3",@"ziran4":@"Natural_4",@"ziran5":@"Natural_5",@"ziran6":@"Natural_6",@"ziran7":@"Natural_7",@"ziran8":@"Natural_8",
                                    @"mitao1":@"Peach_1",@"mitao2":@"Peach_2",@"mitao3":@"Peach_3",@"mitao4":@"Peach_4",@"mitao5":@"Peach_5",@"mitao6":@"Peach_6",@"mitao7":@"Peach_7",@"mitao8":@"Peach_8",
                                    @"zhiganhui1":@"Grey_Texture_1",@"zhiganhui2":@"Grey_Texture_2",@"zhiganhui3":@"Grey_Texture_3",@"zhiganhui4":@"Grey_Texture_4",@"zhiganhui5":@"Grey_Texture_5",@"zhiganhui6":@"Grey_Texture_6",@"zhiganhui7":@"Grey_Texture_7",@"zhiganhui8":@"Grey_Texture_8"
    };

    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSString *str in beautyFiltersDataSource) {
        FUBeautyParam *modle = [[FUBeautyParam alloc] init];
        modle.mParam = str;
        modle.mTitle = [filtersCHName valueForKey:str];
        modle.mValue = 0.4;
        modle.type = FUDataTypeFilter;
        [array addObject:modle];
    }
    
    return array;
}

+(NSArray<FUBeautyParam *>*)setupSkinData{
    
    NSArray *prams = @[@"blur_level",@"color_level",@"red_level",@"sharpen",@"eye_bright",@"tooth_whiten",@"remove_pouch_strength",@"remove_nasolabial_folds_strength"];//
    NSDictionary *titelDic = @{@"blur_level":@"精细磨皮",@"color_level":@"美白",@"red_level":@"红润",@"sharpen":@"锐化",@"remove_pouch_strength":@"去黑眼圈",@"remove_nasolabial_folds_strength":@"去法令纹",@"eye_bright":@"亮眼",@"tooth_whiten":@"美牙"};
    NSDictionary *defaultValueDic = @{@"blur_level":@(0.7),@"color_level":@(0.3),@"red_level":@(0.3),@"sharpen":@(0.2),@"remove_pouch_strength":@(0),@"remove_nasolabial_folds_strength":@(0),@"eye_bright":@(0),@"tooth_whiten":@(0)};
    
    
    NSMutableArray *array = [[NSMutableArray alloc] init];

    for (NSString *str in prams) {

        FUBeautyParam *modle = [[FUBeautyParam alloc] init];
        modle.mParam = str;
        modle.mTitle = [titelDic valueForKey:str];
        modle.mValue = [[defaultValueDic valueForKey:str] floatValue];
        modle.defaultValue = modle.mValue;
        modle.type = FUDataTypeBeautify;
        [array addObject:modle];
        
    }
    
    return array;
    
}

+(NSArray<FUBeautyParam *>*)setupShapData{
   
    NSArray *prams = @[@"cheek_thinning",@"cheek_v",@"cheek_narrow",@"cheek_small",@"eye_enlarging",@"intensity_chin",@"intensity_forehead",@"intensity_nose",@"intensity_mouth",@"intensity_canthus",@"intensity_eye_space",@"intensity_eye_rotate",@"intensity_long_nose",@"intensity_philtrum",@"intensity_smile"];
    NSDictionary *titelDic = @{@"cheek_thinning":@"瘦脸",@"cheek_v":@"v脸",@"cheek_narrow":@"窄脸",@"cheek_small":@"小脸",@"eye_enlarging":@"大眼",@"intensity_chin":@"下巴",
                               @"intensity_forehead":@"额头",@"intensity_nose":@"瘦鼻",@"intensity_mouth":@"嘴型",@"intensity_canthus":@"开眼角",@"intensity_eye_space":@"眼距",@"intensity_eye_rotate":@"眼睛角度",@"intensity_long_nose":@"长鼻",@"intensity_philtrum":@"缩人中",@"intensity_smile":@"微笑嘴角"
    };
   NSDictionary *defaultValueDic = @{@"cheek_thinning":@(0),@"cheek_v":@(0.5),@"cheek_narrow":@(0),@"cheek_small":@(0),@"eye_enlarging":@(0.4),@"intensity_chin":@(0.3),
                              @"intensity_forehead":@(0.3),@"intensity_nose":@(0.5),@"intensity_mouth":@(0.4),@"intensity_canthus":@(0),@"intensity_eye_space":@(0.5),@"intensity_eye_rotate":@(0.5),@"intensity_long_nose":@(0.5),@"intensity_philtrum":@(0.5),@"intensity_smile":@(0)
   };
   
   NSMutableArray *array = [[NSMutableArray alloc] init];
   for (NSString *str in prams) {
       BOOL isStyle101 = NO;
       if ([str isEqualToString:@"intensity_chin"] || [str isEqualToString:@"intensity_forehead"] || [str isEqualToString:@"intensity_mouth"] || [str isEqualToString:@"intensity_eye_space"] || [str isEqualToString:@"intensity_eye_rotate"] || [str isEqualToString:@"intensity_long_nose"] || [str isEqualToString:@"intensity_philtrum"]) {
           isStyle101 = YES;
       }
       
       FUBeautyParam *modle = [[FUBeautyParam alloc] init];
       modle.mParam = str;
       modle.mTitle = [titelDic valueForKey:str];
       modle.mValue = [[defaultValueDic valueForKey:str] floatValue];
       modle.defaultValue = modle.mValue;
       modle.iSStyle101 = isStyle101;
       modle.type = FUDataTypeBeautify;
       [array addObject:modle];
   }
    
    return array;
}


/// 道具贴纸
+(NSArray<FUBeautyParam *>*)setupSticker{
   NSArray *prams = @[@"makeup_noitem",@"sdlu",@"fashi"];//,@"chri1"

   
   NSMutableArray *array = [[NSMutableArray alloc] init];
   for (NSString *str in prams) {
       FUBeautyParam *modle = [[FUBeautyParam alloc] init];
       modle.mParam = str;
//       modle.mTitle = str;
      modle.type = FUDataTypeStrick;
       [array addObject:modle];
       
   }
    
    return array;
}


/// 美妆
+(NSArray<FUBeautyParam *>*)setupMakeupData{
   NSArray *prams = @[@"makeup_noitem",@"chaoA",@"dousha",@"naicha"];
   NSDictionary *titelDic = @{@"makeup_noitem":@"卸妆",@"naicha":@"奶茶",@"dousha":@"豆沙",@"chaoA":@"超A"};
    
   NSMutableArray *array = [[NSMutableArray alloc] init];
   for (NSString *str in prams) {
       FUBeautyParam *modle = [[FUBeautyParam alloc] init];
       modle.mParam = str;
       modle.mTitle = [titelDic valueForKey:str];
       modle.type = FUDataTypeMakeup;
       modle.mValue = 0.7;
       [array addObject:modle];
   }
    
    return array;
}

+(NSArray<FUBeautyParam *>*)setupBodyData{
   NSArray *prams = @[@"BodySlimStrength",@"LegSlimStrength",@"WaistSlimStrength",@"ShoulderSlimStrength",@"HipSlimStrength",@"HeadSlim",@"LegSlim"];
    NSDictionary *titelDic = @{@"BodySlimStrength":@"瘦身",@"LegSlimStrength":@"长腿",@"WaistSlimStrength":@"细腰",@"ShoulderSlimStrength":@"美肩",@"HipSlimStrength":@"美臀",@"HeadSlim":@"小头",@"LegSlim":@"瘦腿"
    };
   NSDictionary *defaultValueDic = @{@"BodySlimStrength":@(0),@"LegSlimStrength":@(0),@"WaistSlimStrength":@(0),@"ShoulderSlimStrength":@(0.5),@"HipSlimStrength":@(0),@"HeadSlim":@(0),@"LegSlim":@(0)
   };
   
   NSMutableArray *array = [[NSMutableArray alloc] init];
   for (NSString *str in prams) {
       BOOL isStyle101 = NO;
       if ([str isEqualToString:@"ShoulderSlimStrength"]) {
           isStyle101 = YES;
       }
       
       FUBeautyParam *modle = [[FUBeautyParam alloc] init];
       modle.mParam = str;
       modle.mTitle = [titelDic valueForKey:str];
       modle.mValue = [[defaultValueDic valueForKey:str] floatValue];
       modle.defaultValue = modle.mValue;
       modle.iSStyle101 = isStyle101;
       modle.type = FUDataTypebody;
       [array addObject:modle];
   }
    
    return array;
}


@end
