import 'package:faceunity_ui/Tools/FUImageTool.dart';
import 'package:faceunity_ui/Models/BaseModel.dart';
import 'package:faceunity_ui/Models/FaceUnityModel.dart';
import 'package:faceunity_ui/ViewModels/BaseViewModel.dart';
import 'package:zego_express_engine/zego_express_engine.dart';

class FUBeautyFilterViewModel extends BaseViewModel {
  FUBeautyFilterViewModel(FaceUnityModel dataModel) : super(dataModel) {
    List<BaseModel> uiList = [];
    Map<String, dynamic> titleAndImagePath = {
      "origin": "原图",
      "bailiang1": "白亮1",
      "bailiang2": "白亮2",
      "bailiang3": "白亮3",
      "bailiang4": "白亮4",
      "bailiang5": "白亮5",
      "bailiang6": "白亮6",
      "bailiang7": "白亮7",
      "fennen1": "粉嫩1",
      "fennen2": "粉嫩2",
      "fennen3": "粉嫩3",
      "fennen4": "粉嫩4",
      "fennen5": "粉嫩5",
      "fennen6": "粉嫩6",
      "fennen7": "粉嫩7",
      "fennen8": "粉嫩8",
      "lengsediao1": "冷色调1",
      "lengsediao2": "冷色调2",
      "lengsediao3": "冷色调3",
      "lengsediao4": "冷色调4",
      "lengsediao5": "冷色调5",
      "lengsediao6": "冷色调6",
      "lengsediao7": "冷色调7",
      "lengsediao8": "冷色调8",
      "lengsediao9": "冷色调9",
      "lengsediao10": "冷色调10",
      "lengsediao11": "冷色调11",
      "ziran1": "自然1",
      "ziran2": "自然2",
      "ziran3": "自然3",
      "ziran4": "自然4",
      "ziran5": "自然5",
      "ziran6": "自然6",
      "ziran7": "自然7",
      "ziran8": "自然8",
      "zhiganhui1": "质感灰1",
      "zhiganhui2": "质感灰2",
      "zhiganhui3": "质感灰3",
      "zhiganhui4": "质感灰4",
      "zhiganhui5": "质感灰5",
      "zhiganhui6": "质感灰6",
      "zhiganhui7": "质感灰7",
      "zhiganhui8": "质感灰8",
    };
    List<String> titlesKey = [
      "origin",
      "ziran1",
      "ziran2",
      "ziran3",
      "ziran4",
      "ziran5",
      "ziran6",
      "ziran7",
      "ziran8",
      "zhiganhui1",
      "zhiganhui2",
      "zhiganhui3",
      "zhiganhui4",
      "zhiganhui5",
      "zhiganhui6",
      "zhiganhui7",
      "zhiganhui8",
      "bailiang1",
      "bailiang2",
      "bailiang3",
      "bailiang4",
      "bailiang5",
      "bailiang6",
      "bailiang7",
      "fennen1",
      "fennen2",
      "fennen3",
      "fennen4",
      "fennen5",
      "fennen6",
      "fennen7",
      "fennen8",
      "lengsediao1",
      "lengsediao2",
      "lengsediao3",
      "lengsediao4",
      "lengsediao5",
      "lengsediao6",
      "lengsediao7",
      "lengsediao8",
      "lengsediao9",
      "lengsediao10",
      "lengsediao11",
    ];
    String commonPre =
        FUImageTool.getImagePathWithRelativePathPre("Asserts/beauty/filter/");
    List<String> imagePaths = List.generate(titlesKey.length, (index) {
      String title = titleAndImagePath[titlesKey[index]];
      return commonPre + title;
    });

    for (var i = 0; i < titlesKey.length; i++) {
      String titleKey = titlesKey[i];
      BaseModel model =
          BaseModel(imagePaths[i], titleAndImagePath[titleKey], 0.4);
      model.ratio = 1.0;
      model.strValue = titleKey;
      uiList.add(model);
    }
    this.dataModel.dataList = uiList;
    //默认选中的索引
    this.selectedIndex = 1;
    this.selectedModel = this.dataModel.dataList[this.selectedIndex];
  }

  @override
  bool showSlider() {
    if (selectedIndex < 1) {
      return false;
    }
    return true;
  }

  @override
  void selectedItem(int index) {
    super.selectedItem(index);
  }

  @override
  //具体选中哪一个由子类决定
  void sliderValueChange(double value) {
    super.sliderValueChange(value);
    FUBeautyPlugin.filterSliderValueChange(
        selectedIndex,
        selectedModel != null ? selectedModel!.value : 0.0,
        selectedModel!.strValue);
  }

  @override
  init() {}
  @override
  dealloc() {}
}
