import 'package:faceunity_ui/Faceunity_ui.dart';
import 'package:flutter/material.dart';
import 'package:zego_express_engine/zego_express_engine.dart';
import 'package:zego_express_engine_example/utils/zego_config.dart';

class CameraScreen extends StatefulWidget {
  final int screenWidthPx;
  final int screenHeightPx;

  CameraScreen(this.screenWidthPx, this.screenHeightPx);

  @override
  State<CameraScreen> createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  Widget? _previewViewWidget;
  int _previewViewID = -1;
  int _playViewID = -1;

  @override
  void initState() {
    super.initState();
    _createEngine();
  }

  void _createEngine() {
    ZegoExpressEngine.createEngine(
        ZegoConfig.instance.appID,
        ZegoConfig.instance.appSign,
        ZegoConfig.instance.isTestEnv,
        ZegoConfig.instance.scenario,
        enablePlatformView: ZegoConfig.instance.enablePlatformView,
        enableFaceUnity: true);

    if (ZegoConfig.instance.enablePlatformView) {
      // Render with PlatformView
      setState(() {
        _previewViewWidget =
            ZegoExpressEngine.instance.createPlatformView((viewID) {
          _previewViewID = viewID;
          _startPreview(_previewViewID);
          // _startPublishingStream(streamID);
        });
      });
    } else {
      // Render with TextureRenderer
      ZegoExpressEngine.instance
          .createTextureRenderer(widget.screenWidthPx, widget.screenHeightPx)
          .then((viewID) {
        _previewViewID = viewID;
        setState(() => _previewViewWidget = Texture(textureId: viewID));
        _startPreview(viewID);
        // _startPublishingStream(streamID);
      });
    }
  }

  @override
  void dispose() {
    ZegoExpressEngine.destroyEngine();
    super.dispose();
  }

  void _startPreview(int viewID) {
    ZegoCanvas canvas = ZegoCanvas.view(viewID);
    ZegoExpressEngine.instance.startPreview(canvas: canvas);
    print('🔌 Start preview, viewID: $viewID');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
      ),
      body: Stack(
        children: [Container(child: _previewViewWidget), FaceunityUI()],
      ),
    );
  }
}
